export enum Mode {
    'accuracy', 
    'medium',
    'performance'
}
export interface ConfigData {
    areaName: string,
    cooldownSetting: number,
    detectionSetting: number,
    mode: Mode
}