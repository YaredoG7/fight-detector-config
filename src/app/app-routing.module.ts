import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

    /**************
     * login path and auth guard will ideally be implemented here
     * lazy loaing is used to show case the concept, this app is less intensive so can be omitted
     *  
     * *************/
    {
      path: '', 
      redirectTo: 'dashbaord',
      pathMatch: 'full'
    },
    {
      path: 'dashbaord',
      loadChildren: () => import('src/app/core/dashboard/dashboard.module').then(m => m.DashboardModule), 

    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
