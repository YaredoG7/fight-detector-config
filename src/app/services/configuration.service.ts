import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { ConfigData } from '../model/configuration.model';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  private dbConfigData: ConfigData = JSON.parse(localStorage.getItem("configData") || "{}");
  private newConfigData = new BehaviorSubject<ConfigData>(this.dbConfigData);

  constructor() { }

  /* save temp data and emit an event when new config parameter is set */
  saveConfiguration(data: ConfigData) {
    localStorage.setItem('configData', JSON.stringify(data))
    this.newConfigData.next(data);
  }

  /* return an observable of recent config parameter */
  newConfigData$(): Observable<ConfigData> {
    return this.newConfigData.asObservable();
  }

}
