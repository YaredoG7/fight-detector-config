import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { ClarityModule, ClrIconModule } from '@clr/angular';
import { RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';
import { NotificationComponent } from './notification/notification.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SideNavComponent,
    NotificationComponent,
  ],
  imports: [
    CommonModule,
    ClrIconModule,
    ClarityModule,
    RouterModule,
    FormsModule
  ], 
  exports: [
    HeaderComponent, 
    SideNavComponent, 
    NotificationComponent,
  ]
})
export class SharedModule { }
