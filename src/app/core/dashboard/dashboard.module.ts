import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { ConfigurationComponent } from './configuration/configuration.component';
import { RouterModule } from '@angular/router';
import { dashboardRoutes } from './dashboard.routing';
import { SharedModule } from '../shared/shared.module';
import { FormsModule} from '@angular/forms';
import { ClarityModule, ClrIconModule } from '@clr/angular';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [DashboardComponent, ConfigurationComponent],
  imports: [
    CommonModule,
    SharedModule,
    ClarityModule,
    ClrIconModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(dashboardRoutes)
  ]
})
export class DashboardModule { }
