import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfigData } from 'src/app/model/configuration.model';
import { FormBuilder } from '@angular/forms';
import { ConfigurationService } from 'src/app/services/configuration.service';
import { isEqual } from 'src/app/core/utils/object.comparison';
import { customAnimations } from 'src/app/core/utils/animation.definition';
import { SubSink } from 'subsink'

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.scss'],
  animations: [customAnimations]
})
export class ConfigurationComponent implements OnInit, OnDestroy {
  
  public configData: ConfigData = <ConfigData>{};
  private dbConfigData: ConfigData = <ConfigData>{};
  public configChanged: boolean = false;
  readonly detectionAccuracy: number = 200;
  /*  put all subscriptions in the sink to unsubscribe when component is destroyed*/
  private subs = new SubSink();

  constructor(
    private fb: FormBuilder,
    private apiService: ConfigurationService
  ) {}

  configForm = this.fb.group({
    areaname: [],
    cooldown: [],
    detectiontime: [],
    mode: []
  });

  ngOnInit(): void {
    this.subs.sink = this.apiService.newConfigData$().subscribe(data => {
    // prevent logging error if local storage is empty
      if (Object.values(data).length > 0) {
        this.configForm.setValue(data);
      }
      this.dbConfigData = data;
    });
    this.subs.sink = this.configForm.valueChanges.subscribe(selectedValue => {
      this.configChanged = !isEqual(selectedValue, this.dbConfigData);
    });
  }

  /* used in HTML */
  aboveCooldown(): boolean {
    return this.configForm.value.detectiontime > this.configForm.value.cooldown;
  }

  saveConfig() {
    this.apiService.saveConfiguration(this.configForm.value);
    /* visual notification is ideal to show successful operation, 
       toggeling is used due to backend unavailability */
    this.configChanged = !this.configChanged;
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
