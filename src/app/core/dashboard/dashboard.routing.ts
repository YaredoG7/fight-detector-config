import { DashboardComponent } from './dashboard.component';
import { ConfigurationComponent } from './configuration/configuration.component';

export const dashboardRoutes = [
    {
      path: '',
      component: DashboardComponent, 
      children: [
          {
              path: 'configuration', 
              component: ConfigurationComponent
          }
      ]
    }, 
   
];


