import { trigger, state, style, transition, animate } from '@angular/animations';

export const customAnimations = [
    trigger('flyUpDown', [
        state('1', style({ transform: 'translateY(0)' })),
        transition('void => *', [
          style({ transform: 'translateY(-100%)' }),
          animate('350ms ease-out')
        ]),
        transition('* => void', [
          style({ transform: 'translateY(0)' }),
          animate('350ms ease-in')
        ])
      ]),
]